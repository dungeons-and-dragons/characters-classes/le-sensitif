# LeSensitif

Le Sensitif est une classe de personnage pour Advanced Dungeons & Dragons, basée sur l'utilisation de pouvoirs et aptitudes aux contrôles de la pensée et des émotions. C'est un personnage généralement assez subtile à jouer et très intéressant lorsqu'on veut explorer le monde des personnages dotés de pouvoirs psychiques (psionics).

Cette classe de personnage a été créée dans les années 80 par un des joueurs fréquentant le club de JDR de Saint-Rémy-lès-Chevreuse.

Cette version de la classe de personnage Le Sensitif est une correction et un enrichissement de la version originale.

Brunus.
